<a name="v0.1.2"></a>
### v0.1.2 (2015-08-25)


#### Bug Fixes

* **Symlinks:**  adds ability to optionally follow symlinks while counting ([d265980e](https://github.com/kbknapp/cargo-count/commit/d265980e8e06101c07dd3265dd2d66d834b09c58), closes [#6](https://github.com/kbknapp/cargo-count/issues/6), [#7](https://github.com/kbknapp/cargo-count/issues/7))



<a name="v0.1.1"></a>
### v0.1.1 (2015-08-24)


#### Bug Fixes

*   fixes unsafe code count bug in C and C++ files ([1c1e01d6](https://github.com/kbknapp/cargo-count/commit/1c1e01d67c0f5ad717b3842295c5fb597db65656), closes [#1](https://github.com/kbknapp/cargo-count/issues/1))
*   fixes single line block comment bug ([d896412b](https://github.com/kbknapp/cargo-count/commit/d896412bf81da6271c762ab5168d40e27e8eb988), closes [#2](https://github.com/kbknapp/cargo-count/issues/2))
* **Unsafe Counter:**  fixes a bug in the unsafe counter for Rust giving incorrect numbers ([317d2fc9](https://github.com/kbknapp/cargo-count/commit/317d2fc9964d131dbdc28fa93a6e29230143cb94), closes [#5](https://github.com/kbknapp/cargo-count/issues/5))



<a name="v0.1.0"></a>
## v0.1.0 (2015-08-21)


#### Documentation

*   adds a changelog using clog ([34fdc52b](https://github.com/kbknapp/cargo-count/commit/34fdc52b8dac02b5668a0cd9daca57ae3dd9de17))
*   updates the readme ([d302d65d](https://github.com/kbknapp/cargo-count/commit/d302d65da7614c609120011858cee0cc4e32bcc3))

#### Features

*   initial implementation ([b6e968fb](https://github.com/kbknapp/cargo-count/commit/b6e968fb2c1ff0bc5af6b21a11f83099c6fe6e68))

